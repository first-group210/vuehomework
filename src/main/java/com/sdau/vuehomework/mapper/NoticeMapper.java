package com.sdau.vuehomework.mapper;

import com.sdau.vuehomework.entity.Notice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
