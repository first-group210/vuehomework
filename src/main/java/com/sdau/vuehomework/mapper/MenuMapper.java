package com.sdau.vuehomework.mapper;

import com.sdau.vuehomework.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
