package com.sdau.vuehomework.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sdau.vuehomework.common.lang.Const;
import com.sdau.vuehomework.common.lang.Result;
import com.sdau.vuehomework.entity.Menu;
import com.sdau.vuehomework.entity.Role;
import com.sdau.vuehomework.entity.RoleMenu;
import com.sdau.vuehomework.entity.UserRole;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/sys/role")
public class RoleController extends BaseController {

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAuthority('sys:role:list')")
    public Result info(@PathVariable("id") Long id){

        Role role = roleService.getById(id);
        //获取角色相关联的菜单id
        List<RoleMenu> roleMenuList = roleMenuService.list(new QueryWrapper<RoleMenu>().eq("role_id", id));
        List<Long> menuIds = roleMenuList.stream().map(p -> p.getMenuId()).collect(Collectors.toList());

        role.setMenuIds(menuIds);
        return Result.succ(role);
    }

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:role:list')")
    public Result list(String name){

        Page<Role> pageData = roleService.page(getPage(),
                new QueryWrapper<Role>()
                        .like(StrUtil.isNotBlank(name), "name", name));
        return Result.succ(pageData);
    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:role:save')")
    public Result save(@Validated @RequestBody Role role){
        role.setCreated(LocalDateTime.now());
        role.setStatu(Const.STATUS_ON);

        roleService.save(role);
        return Result.succ(role);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:role:update')")
    public Result update(@Validated @RequestBody Role role){
        role.setUpdated(LocalDateTime.now());
        roleService.updateById(role);

        //更新缓存
        userService.clearUserAuthorityInofByRoleId(role.getId());

        return Result.succ(role);
    }

    @Transactional
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('sys:role:delete')")
    public Result info(@RequestBody Long[] ids){
        roleService.removeByIds(Arrays.asList(ids));

        //中间表删除
        userRoleService.remove(new QueryWrapper<UserRole>().in("role_id", ids));
        roleMenuService.remove(new QueryWrapper<RoleMenu>().in("role_id", ids));

        //缓存同步删除
        Arrays.stream(ids).forEach(id -> {
            //更新缓存
            userService.clearUserAuthorityInofByRoleId(id);
        });

        return Result.succ("");
    }

    @Transactional
    @PostMapping("/perm/{roleId}")
    @PreAuthorize("hasAuthority('sys:role:perm')")
    public Result info(@PathVariable("roleId") Long roleId, @RequestBody Long[] menusIds){
        List<RoleMenu> roleMenus = new ArrayList<>();

        Arrays.stream(menusIds).forEach(menuId -> {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setMenuId(menuId);
            roleMenu.setRoleId(roleId);

            roleMenus.add(roleMenu);
        });

        //先删除原来的记录，在保存新的
        roleMenuService.remove(new QueryWrapper<RoleMenu>().eq("role_id", roleId));
        roleMenuService.saveBatch(roleMenus);

        //删除缓存
        userService.clearUserAuthorityInofByRoleId(roleId);

        return Result.succ(menusIds);
    }

}
