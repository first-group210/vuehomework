package com.sdau.vuehomework.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sdau.vuehomework.common.lang.Const;
import com.sdau.vuehomework.common.lang.Result;
import com.sdau.vuehomework.entity.Class;
import com.sdau.vuehomework.entity.Role;
import com.sdau.vuehomework.entity.RoleMenu;
import com.sdau.vuehomework.entity.UserRole;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
@RestController
@RequestMapping("/sys/class")
public class ClassController extends BaseController {

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAuthority('sys:class:list')")
    public Result info(@PathVariable("id") Long id){

        Role role = roleService.getById(id);
        //获取角色相关联的菜单id
        List<RoleMenu> roleMenuList = roleMenuService.list(new QueryWrapper<RoleMenu>().eq("role_id", id));
        List<Long> menuIds = roleMenuList.stream().map(p -> p.getMenuId()).collect(Collectors.toList());

        role.setMenuIds(menuIds);
        return Result.succ(role);
    }

    @GetMapping("/list")
    public Result list(String name){

        Page<Class> pageData = classService.page(getPage(),
                new QueryWrapper<Class>()
                        .like(StrUtil.isNotBlank(name), "name", name));
        List<Class> classList = pageData.getRecords();
        for (Class c : classList) {
            c.setTeachername(userService.getById(c.getTeacherId()).getUsername());
        }
        pageData.setRecords(classList);
        return Result.succ(pageData);
    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:class:save')")
    public Result save(@Validated @RequestBody Class cl){
        /*role.setCreated(LocalDateTime.now());
        role.setStatu(Const.STATUS_ON);
        roleService.save(role);*/
        cl.setCreated(LocalDateTime.now());
        cl.setStatu(Const.STATUS_ON);
        classService.save(cl);
        return Result.succ(cl);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:class:update')")
    public Result update(@Validated @RequestBody Class cl){
        /*role.setUpdated(LocalDateTime.now());
        roleService.updateById(role);
        //更新缓存
        userService.clearUserAuthorityInofByRoleId(role.getId());*/
        cl.setUpdated(LocalDateTime.now());
        classService.updateById(cl);

        return Result.succ(cl);
    }

    @Transactional
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('sys:class:delete')")
    public Result info(@RequestBody Long[] ids){
        classService.removeByIds(Arrays.asList(ids));


        return Result.succ("");
    }

    @Transactional
    @PostMapping("/perm/{roleId}")
    @PreAuthorize("hasAuthority('sys:class:perm')")
    public Result info(@PathVariable("roleId") Long roleId, @RequestBody Long[] menusIds){
        List<RoleMenu> roleMenus = new ArrayList<>();

        Arrays.stream(menusIds).forEach(menuId -> {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setMenuId(menuId);
            roleMenu.setRoleId(roleId);

            roleMenus.add(roleMenu);
        });

        //先删除原来的记录，在保存新的
        roleMenuService.remove(new QueryWrapper<RoleMenu>().eq("role_id", roleId));
        roleMenuService.saveBatch(roleMenus);

        //删除缓存
        userService.clearUserAuthorityInofByRoleId(roleId);

        return Result.succ(menusIds);
    }
}
