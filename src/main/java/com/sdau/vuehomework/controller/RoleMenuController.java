package com.sdau.vuehomework.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/role-menu")
public class RoleMenuController extends BaseController {

}
