package com.sdau.vuehomework.controller;


import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sdau.vuehomework.common.dto.MenuDto;
import com.sdau.vuehomework.common.lang.Result;
import com.sdau.vuehomework.entity.Menu;
import com.sdau.vuehomework.entity.RoleMenu;
import com.sdau.vuehomework.entity.User;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/sys/menu")
public class MenuController extends BaseController {

    /**
     * 用户当前用户的菜单和权限
     * @param principal
     * @return
     */
    @GetMapping("/nav")
    public Result nav(Principal principal) {
        User user = userService.getByUsername(principal.getName());

        //获取权限信息
        String authorityInfo = userService.getUserAuthorityInfo(user.getId());
        String[] authorityInfoArray = StringUtils.tokenizeToStringArray(authorityInfo, ",");


        //获取导航栏信息
        List<MenuDto> navs = menuService.getCurrentUserNav();

        System.out.println("authoritys" + authorityInfoArray);
        System.out.println("nav" + navs);

        return Result.succ(MapUtil.builder()
                .put("authoritys", authorityInfoArray)
                .put("nav", navs)
                .map()
        );
    }

    @GetMapping("/info/{id}")
    public Result info(@PathVariable("id") Long id) {
        return Result.succ(menuService.getById(id));
    }

    @GetMapping("/list")
    public Result list() {
        List<Menu> menus = menuService.tree();
        return Result.succ(menus);
    }

    @PostMapping("/save")
    public Result save(@Validated @RequestBody Menu menu) {

        menu.setCreated(LocalDateTime.now());
        menuService.save(menu);
        return Result.succ(menu);
    }

    @PostMapping("/update")
    public Result update(@Validated @RequestBody Menu menu) {

        menu.setUpdated(LocalDateTime.now());
        menuService.updateById(menu);
        // 清除所有与菜单相关的权限缓存
        userService.clearUserAuthorityInofByMenuId(menu.getId());
        return Result.succ(menu);
    }

    @PostMapping("/delete/{id}")
    public Result delete(@PathVariable("id") Long id) {

        int count = menuService.count(new QueryWrapper<Menu>().eq("parent_id", id));
        if (count > 0) {
            return Result.fail("请先删除子菜单");
        }
        userService.clearUserAuthorityInofByMenuId(id);
        menuService.removeById(id);
        //同步删除中间关联表
        roleMenuService.remove(new QueryWrapper<RoleMenu>().eq("menu_id", id));
        return Result.succ("");
    }

}
