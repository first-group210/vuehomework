package com.sdau.vuehomework.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sdau.vuehomework.common.dto.HomeworkDto;
import com.sdau.vuehomework.common.lang.Const;
import com.sdau.vuehomework.common.lang.Result;
import com.sdau.vuehomework.entity.*;
import org.springframework.data.redis.connection.ReactiveHashCommands;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
@RestController
@RequestMapping("/sys/homework")
public class HomeworkController extends BaseController {

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAuthority('sys:homework:list')")
    public Result info(@PathVariable("id") Long id){

        Role role = roleService.getById(id);
        //获取角色相关联的菜单id
        List<RoleMenu> roleMenuList = roleMenuService.list(new QueryWrapper<RoleMenu>().eq("role_id", id));
        List<Long> menuIds = roleMenuList.stream().map(p -> p.getMenuId()).collect(Collectors.toList());

        role.setMenuIds(menuIds);
        return Result.succ(role);
    }

    @GetMapping("/list")
    public Result list() {
        List<Homework> homeworks = homeworkService.tree();
        List<HomeworkDto> homeworkDtos = convert(homeworks);
        return Result.succ(homeworkDtos);
    }

    public List<HomeworkDto> convert(List<Homework> homeworkTree) {
        List<HomeworkDto> homeworkDtos = new ArrayList<>();

        for (Homework h : homeworkTree) {
            HomeworkDto homeworkDto = new HomeworkDto();

            homeworkDto.setId(h.getId());
            homeworkDto.setTitle(h.getTitle());
            homeworkDto.setDetail(h.getDetail());
            homeworkDto.setClassname(classService.getById(h.getClassId()).getClassname());
            if (h.getStudentId() != null)
                homeworkDto.setStudentname(userService.getById(h.getStudentId()).getUsername());
            homeworkDto.setFinalFile(h.getFinalFile());
            homeworkDto.setRemark(h.getRemark());
            homeworkDto.setStatu(h.getStatu());

            if (h.getChildren().size() > 0) {
                homeworkDto.setChildren(convert(h.getChildren()));
            }
            homeworkDtos.add(homeworkDto);
        }
        return homeworkDtos;
    }

//    @GetMapping("/list")
//    @PreAuthorize("hasAuthority('sys:role:list')")
//    public Result list(String name){
//
//        List<Homework> homeworkList = homeworkService.tree();
//        Page<Homework> pageData = homeworkService.page(getPage(),
//                new QueryWrapper<Homework>()
//                        .like(StrUtil.isNotBlank(name), "name", name));
//        List<Homework> homeworks = pageData.getRecords();
//        pageData.setRecords(homeworkList);
//        return Result.succ(pageData);
//    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:homework:save')")
    public Result save(@Validated @RequestBody Role role){
        role.setCreated(LocalDateTime.now());
        role.setStatu(Const.STATUS_ON);

        roleService.save(role);
        return Result.succ(role);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:homework:update')")
    public Result update(@Validated @RequestBody Role role){
        role.setUpdated(LocalDateTime.now());
        roleService.updateById(role);

        //更新缓存
        userService.clearUserAuthorityInofByRoleId(role.getId());

        return Result.succ(role);
    }

    @Transactional
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('sys:homework:delete')")
    public Result info(@RequestBody Long[] ids){
        roleService.removeByIds(Arrays.asList(ids));

        //中间表删除
        userRoleService.remove(new QueryWrapper<UserRole>().in("role_id", ids));
        roleMenuService.remove(new QueryWrapper<RoleMenu>().in("role_id", ids));

        //缓存同步删除
        Arrays.stream(ids).forEach(id -> {
            //更新缓存
            userService.clearUserAuthorityInofByRoleId(id);
        });

        return Result.succ("");
    }

    @Transactional
    @PostMapping("/perm/{roleId}")
    @PreAuthorize("hasAuthority('sys:homework:perm')")
    public Result info(@PathVariable("roleId") Long roleId, @RequestBody Long[] menusIds){
        List<RoleMenu> roleMenus = new ArrayList<>();

        Arrays.stream(menusIds).forEach(menuId -> {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setMenuId(menuId);
            roleMenu.setRoleId(roleId);

            roleMenus.add(roleMenu);
        });

        //先删除原来的记录，在保存新的
        roleMenuService.remove(new QueryWrapper<RoleMenu>().eq("role_id", roleId));
        roleMenuService.saveBatch(roleMenus);

        //删除缓存
        userService.clearUserAuthorityInofByRoleId(roleId);

        return Result.succ(menusIds);
    }
}
