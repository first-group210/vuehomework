package com.sdau.vuehomework.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sdau.vuehomework.common.dto.PassDto;
import com.sdau.vuehomework.common.lang.Const;
import com.sdau.vuehomework.common.lang.Result;
import com.sdau.vuehomework.entity.Role;
import com.sdau.vuehomework.entity.RoleMenu;
import com.sdau.vuehomework.entity.User;
import com.sdau.vuehomework.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/sys/user")
public class UserController extends BaseController {

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAuthority('sys:user:list')")
    public Result info(@PathVariable("id") Long id){

        System.out.println("user-----info------");
        User user = userService.getById(id);
        Assert.notNull(user, "找不到该管理员");

        List<Role> roles = roleService.listRolesByUserId(id);

        user.setRoles(roles);
        return Result.succ(user);
    }

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:user:list')")
    public Result list(String username){
        System.out.println("user-----list------");
        Page<User> pageData = userService.page(getPage(), new QueryWrapper<User>()
                        .like(StrUtil.isNotBlank(username), "username", username));

        pageData.getRecords().forEach(u -> {
            u.setRoles(roleService.listRolesByUserId(u.getId()));
        });

        return Result.succ(pageData);
    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:user:save')")
    public Result save(@Validated @RequestBody User user){
        System.out.println("user-----save------");
        user.setCreated(LocalDateTime.now());
        user.setStatu(Const.STATUS_ON);

        String password = passwordEncoder.encode(Const.DEFULT_PASSWORD);
        user.setPassword(password);

        //默认头像
        user.setAvatar(Const.DEFULT_AVATAR);

        userService.save(user);

        return Result.succ(user);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:user:update')")
    public Result update(@Validated @RequestBody User user){
        System.out.println("user-----update------");
        user.setUpdated(LocalDateTime.now());

        userService.updateById(user);

        return Result.succ(user);
    }

    @Transactional
    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('sys:user:delete')")
    public Result delete(@RequestBody Long[] ids){
        System.out.println("user-----delete------");
        userService.removeByIds(Arrays.asList(ids));
        userRoleService.remove(new QueryWrapper<UserRole>().in("user_id", ids));
        return Result.succ("");
    }

    @Transactional
    @PostMapping("/role/{user_id}")
    @PreAuthorize("hasAuthority('sys:user:role')")
    public Result rolePerm(@PathVariable("user_id") Long userId, @RequestBody Long[] roleIds) {

        System.out.println("user-----rolePerm------");
        List<UserRole> userRoles = new ArrayList<>();
        Arrays.asList(roleIds).forEach(r -> {
            UserRole userRole = new UserRole();
            userRole.setRoleId(r);
            userRole.setUserId(userId);

            userRoles.add(userRole);
        });

        userRoleService.remove(new QueryWrapper<UserRole>().eq("user_id", userId));
        userRoleService.saveBatch(userRoles);

        //删除缓存
        User user = userService.getById(userId);
        userService.clearUserAuthorityInof(user.getUsername());

        return Result.succ("");
    }

    @PostMapping("/repass")
    @PreAuthorize("hasAuthority('sys:user:repass')")
    public Result repass(@RequestBody Long userId) {
        System.out.println("user-----repass------");
        User user = userService.getById(userId);

        user.setPassword(passwordEncoder.encode(Const.DEFULT_PASSWORD));
        user.setUpdated(LocalDateTime.now());

        userService.updateById(user);
        return Result.succ("");
    }

    @PostMapping("/updatepass")
    public Result updatepass(@Validated @RequestBody PassDto passDto, Principal principal) {
        System.out.println("user-----updatepass------");
        User user = userService.getByUsername(principal.getName());

        boolean matches = passwordEncoder.matches(passDto.getCurrentPass(), user.getPassword());
        if (!matches) {
            return Result.fail("旧密码不正确");
        }

        user.setPassword(passwordEncoder.encode(passDto.getPassword()));
        user.setUpdated(LocalDateTime.now());

        userService.updateById(user);
        return Result.succ("");
    }

}
