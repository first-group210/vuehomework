package com.sdau.vuehomework.common.dto;

import com.sdau.vuehomework.entity.Homework;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class HomeworkDto implements Serializable {
    private Long id;

    private String title;

    private String detail;

    private String classname;

    private String studentname;

    private String finalFile;

    private String remark;

    private Integer statu;

    private List<HomeworkDto> children = new ArrayList<>();

}
