package com.sdau.vuehomework.common.lang;

public class Const {

    public final static String CAPTCHA_KEY = "captcha";

    public final static Integer STATUS_ON = 1;
    public final static Integer STATUS_OFF = 0;
    
    public final static String DEFULT_PASSWORD = "123456";
    public static final String DEFULT_AVATAR = "https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png";
}
