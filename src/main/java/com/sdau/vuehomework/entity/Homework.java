package com.sdau.vuehomework.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_homework")
public class Homework extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long parentId;

    private String title;

    private String detail;

    private Integer classId;

    private Integer studentId;

    private String finalFile;

    @TableField("orderNum")
    private Integer ordernum;

    private String remark;

    @TableField(exist = false)
    private List<Homework> children = new ArrayList<>();

    private Integer statu;
}
