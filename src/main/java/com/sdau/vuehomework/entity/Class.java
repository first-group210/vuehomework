package com.sdau.vuehomework.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_class")
public class Class extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String classname;

    private String grade;

    private Integer teacherId;

    @TableField(exist = false)
    private String teachername;
}
