package com.sdau.vuehomework.security;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.sdau.vuehomework.common.exception.CaptchaException;
import com.sdau.vuehomework.common.lang.Const;
import com.sdau.vuehomework.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CaptchaFilter extends OncePerRequestFilter {

	@Autowired
	RedisUtil redisUtil;

	@Autowired
	LoginFailureHandler loginFailureHandler;

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

		System.out.println("CaptchaFilter----------------doFilterInternal-------");
		String url = httpServletRequest.getRequestURI();

		System.out.println("url==== " + url);
		if ("/login".equals(url) && httpServletRequest.getMethod().equals("POST")) {

			try{
				System.out.println("校验验证码---------");
				// 校验验证码
				validate(httpServletRequest);
			} catch (CaptchaException e) {

				System.out.println("loginFailureHandler---------------");
				// 交给认证失败处理器
				loginFailureHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, e);
			}
		}

		filterChain.doFilter(httpServletRequest, httpServletResponse);
	}

	// 校验验证码逻辑
	private void validate(HttpServletRequest httpServletRequest) {

//		System.out.println("验证验证码是否正确中......");
		System.out.println("CaptchaFilter----------------validate-------");
		String code = httpServletRequest.getParameter("code");
		String key = httpServletRequest.getParameter("token");
		System.out.println("code：" + code);
		System.out.println("token: " + key);


		if (StringUtils.isBlank(code) || StringUtils.isBlank(key)) {
			System.out.println("key/code为空...");
			throw new CaptchaException("验证码错误");
		}

		if (!code.equals(redisUtil.hget(Const.CAPTCHA_KEY, key))) {
			System.out.println("redisUtil.code:" + redisUtil.hget(Const.CAPTCHA_KEY, key));
			throw new CaptchaException("验证码错误");
		}

		// 一次性使用
		redisUtil.hdel(Const.CAPTCHA_KEY, key);
	}
}
