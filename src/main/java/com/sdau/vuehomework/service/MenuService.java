package com.sdau.vuehomework.service;

import com.sdau.vuehomework.common.dto.MenuDto;
import com.sdau.vuehomework.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
public interface MenuService extends IService<Menu> {
    List<MenuDto> getCurrentUserNav();

    List<Menu> tree();
}
