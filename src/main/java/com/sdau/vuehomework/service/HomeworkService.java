package com.sdau.vuehomework.service;

import com.sdau.vuehomework.entity.Homework;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
public interface HomeworkService extends IService<Homework> {

    List<Homework> tree();
}
