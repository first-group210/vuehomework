package com.sdau.vuehomework.service;

import com.sdau.vuehomework.entity.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
