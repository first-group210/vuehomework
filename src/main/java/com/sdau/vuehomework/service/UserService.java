package com.sdau.vuehomework.service;

import com.sdau.vuehomework.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
public interface UserService extends IService<User> {

    public User getByUsername(String username);

    public String getUserAuthorityInfo(Long userId);

    void clearUserAuthorityInof(String username);

    void clearUserAuthorityInofByRoleId(Long roleId);

    void clearUserAuthorityInofByMenuId(Long menuId);

}
