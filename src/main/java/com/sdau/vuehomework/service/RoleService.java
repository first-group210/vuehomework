package com.sdau.vuehomework.service;

import com.sdau.vuehomework.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
public interface RoleService extends IService<Role> {

    List<Role> listRolesByUserId(Long userId);
}
