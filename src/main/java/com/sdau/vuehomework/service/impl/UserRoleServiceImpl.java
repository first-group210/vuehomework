package com.sdau.vuehomework.service.impl;

import com.sdau.vuehomework.entity.UserRole;
import com.sdau.vuehomework.mapper.UserRoleMapper;
import com.sdau.vuehomework.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
