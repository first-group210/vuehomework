package com.sdau.vuehomework.service.impl;

import com.sdau.vuehomework.entity.Class;
import com.sdau.vuehomework.mapper.ClassMapper;
import com.sdau.vuehomework.service.ClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
@Service
public class ClassServiceImpl extends ServiceImpl<ClassMapper, Class> implements ClassService {

}
