package com.sdau.vuehomework.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sdau.vuehomework.entity.Menu;
import com.sdau.vuehomework.entity.Role;
import com.sdau.vuehomework.entity.User;
import com.sdau.vuehomework.mapper.UserMapper;
import com.sdau.vuehomework.service.MenuService;
import com.sdau.vuehomework.service.RoleService;
import com.sdau.vuehomework.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sdau.vuehomework.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    RoleService roleService;

    @Autowired
    UserMapper userMapper;

    @Lazy
    @Autowired
    MenuService menuService;

    @Autowired
    RedisUtil redisUtil;

    @Override
    public User getByUsername(String username) {
        return getOne(new QueryWrapper<User>().eq("username", username));
    }

    @Override
    public String getUserAuthorityInfo(Long userId) {

        User user = userMapper.selectById(userId);
        // ROLE_....
        String authority = "";

        if (redisUtil.hasKey("GrantedAuthority:" +  user.getUsername())){
            authority = (String) redisUtil.get("GrantedAuthority:" +  user.getUsername());
            System.out.println("获取缓存中的数据");
        } else {
            System.out.println("获取数据中......");
            //获取角色
            List<Role> roles = roleService.list(new QueryWrapper<Role>()
                    .inSql("id", "select role_id from sys_user_role where user_id = " + userId));
            if (roles.size() > 0) {
                String roleCodes = roles.stream().map(r -> "ROLE_" + r.getCode()).collect(Collectors.joining(","));
                authority = roleCodes.concat(",");
            }

            //获取菜单操作编码
            List<Long> menuIds = userMapper.getNavMenuIds(userId);
            if (menuIds.size() > 0) {
                List<Menu> menus = menuService.listByIds(menuIds);
                String menuPerms = menus.stream().map(m -> m.getPerms()).collect(Collectors.joining(","));

                authority = authority.concat(menuPerms);
            }
        }



        redisUtil.set("GrantedAuthority:" +  user.getUsername(), authority, 60 * 60);


        return authority;
    }

    @Override
    public void clearUserAuthorityInof(String username) {
        redisUtil.del("GrantedAuthority:" + username);
    }

    @Override
    public void clearUserAuthorityInofByRoleId(Long roleId) {

        List<User> users = this.list(new QueryWrapper<User>()
                .inSql("id", "select user_id from sys_user_role where role_id = " + roleId));
        users.forEach(user -> {
            this.clearUserAuthorityInof(user.getUsername());
        });
    }

    @Override
    public void clearUserAuthorityInofByMenuId(Long menuId) {
        List<User> users = userMapper.listByMenuId(menuId);
        users.forEach(user -> {
            this.clearUserAuthorityInof(user.getUsername());
        });
    }
}
