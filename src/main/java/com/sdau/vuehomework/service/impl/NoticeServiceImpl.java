package com.sdau.vuehomework.service.impl;

import com.sdau.vuehomework.entity.Notice;
import com.sdau.vuehomework.mapper.NoticeMapper;
import com.sdau.vuehomework.service.NoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

}
