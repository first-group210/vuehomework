package com.sdau.vuehomework.service.impl;

import com.sdau.vuehomework.entity.RoleMenu;
import com.sdau.vuehomework.mapper.RoleMenuMapper;
import com.sdau.vuehomework.service.RoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
