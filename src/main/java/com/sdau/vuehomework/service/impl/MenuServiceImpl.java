package com.sdau.vuehomework.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sdau.vuehomework.common.dto.MenuDto;
import com.sdau.vuehomework.entity.Menu;
import com.sdau.vuehomework.entity.User;
import com.sdau.vuehomework.mapper.MenuMapper;
import com.sdau.vuehomework.mapper.UserMapper;
import com.sdau.vuehomework.security.UserDetailServiceImpl;
import com.sdau.vuehomework.service.MenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sdau.vuehomework.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lei
 * @since 2022-04-24
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Autowired
    UserService userService;

    @Autowired
    UserMapper userMapper;


    @Override
    public List<MenuDto> getCurrentUserNav() {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getByUsername(username);

        List<Long> menuIds = userMapper.getNavMenuIds(user.getId());
        List<Menu> menus = this.listByIds(menuIds);

        //转树状结构
        List<Menu> menuTree = buildTreeMenu(menus);

        //实体转DTO
        return convert(menuTree);
    }

    @Override
    public List<Menu> tree() {
        //获取所有菜单信息
        List<Menu> menus = this.list(new QueryWrapper<Menu>().orderByAsc("orderNum"));

        //转成树形结构
        return buildTreeMenu(menus);
    }

    private List<MenuDto> convert(List<Menu> menuTree) {
        List<MenuDto> menuDtos = new ArrayList<>();

        menuTree.forEach(m -> {
            MenuDto dto = new MenuDto();

            dto.setId(m.getId());
            dto.setName(m.getPerms());
            dto.setTitle(m.getName());
            dto.setPath(m.getPath());
            dto.setComponent(m.getComponent());

            if (m.getChildren().size() > 0) {
                dto.setChildren(convert(m.getChildren()));
            }
            menuDtos.add(dto);
        });
        return menuDtos;
    }

    private List<Menu> buildTreeMenu(List<Menu> menus) {
        List<Menu> finalMenus = new ArrayList<>();

        //先各自寻找到各自的孩子
        for (Menu menu : menus) {

            for (Menu e : menus) {
                if (menu.getId() == e.getParentId()) {
                    menu.getChildren().add(e);
                }
            }

            //提取出父节点
            if (menu.getParentId() == 0L) {
                finalMenus.add(menu);
            }
        }
        System.out.println(JSONUtil.toJsonStr(finalMenus));
        return finalMenus;
    }
}
