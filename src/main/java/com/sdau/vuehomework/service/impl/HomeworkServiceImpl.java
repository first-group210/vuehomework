package com.sdau.vuehomework.service.impl;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sdau.vuehomework.entity.Homework;
import com.sdau.vuehomework.mapper.HomeworkMapper;
import com.sdau.vuehomework.service.HomeworkService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
@Service
public class HomeworkServiceImpl extends ServiceImpl<HomeworkMapper, Homework> implements HomeworkService {

    @Override
    public List<Homework> tree() {
        List<Homework> homeworkList = this.list(new QueryWrapper<Homework>().orderByDesc("orderNum"));


        return buildTreeHomework(homeworkList);
    }

    private List<Homework> buildTreeHomework(List<Homework> homeworkList) {
        List<Homework> finalHomeworks = new ArrayList<>();

        //先各自寻找到各自的孩子
        for (Homework homework : homeworkList) {
            for (Homework h : homeworkList) {
                if (homework.getId() == h.getParentId()) {
                    homework.getChildren().add(h);
                }
            }

            //提取出父节点
            if (homework.getParentId() == 0L) {
                finalHomeworks.add(homework);
            }
        }
        System.out.println(JSONUtil.toJsonStr(finalHomeworks));
        return finalHomeworks;
    }
}
