package com.sdau.vuehomework.service;

import com.sdau.vuehomework.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lei
 * @since 2022-05-07
 */
public interface NoticeService extends IService<Notice> {

}
