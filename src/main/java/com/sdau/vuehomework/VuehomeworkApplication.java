package com.sdau.vuehomework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication
@EnableOpenApi
public class VuehomeworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(VuehomeworkApplication.class, args);
    }

}
